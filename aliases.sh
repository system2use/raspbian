alias apt-update="sudo apt-get update"
alias apt-search='sudo aptitude search'
alias apt-install='sudo apt-get install -y --force-yes'
alias apt-remove='sudo apt-get remove -y --force-yes'
alias apt-clean='sudo apt-get -y --force-yes autoremove && apt-get autoclean'
alias apt-upgrade='sudo apt-get -y update && aptitude -y safe-upgrade'

