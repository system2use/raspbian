export APT_PKGs="aptitude mksh zsh zsh-lovers"
export APT_PKGs=$APT_PKGs" etckeeper"
export APT_PKGs=$APT_PKGs" htop fping nmap traceroute"
export APT_PKGs=$APT_PKGs" wvdial hostapd tor miredo i2p"
export APT_PKGs=$APT_PKGs" git-all python-dev libxml2-dev libxslt1-dev"
export APT_PKGs=$APT_PKGs" python-lxml python-smbus"
export APT_PKGs=$APT_PKGs" i2c-tools"

export PYPI_PKGs=$PYPI_PKGs" rpi.gpio requests"

