#!/bin/bash

echo "Shelter bootstrap for Debian & Ubuntu :"
echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"

#wget -c $pkgs

/shl/boot/os/debian/sources.sh

apt-install $APT_PKGs

#for key in `echo sysctl.conf {before,after}.rules` ; do
#    ln -sf $SHL_ROOT/etc/linux/ufw/$key /etc/ufw/$key
#done

ln -sf $SHL_ROOT/boot/cron /etc/cron.d/shelter
ln -sf $SHL_ROOT/boot/motd /etc/motd

