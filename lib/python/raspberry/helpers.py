#-*- coding: utf-8 -*-

from sparkle.shortcuts import *

################################################################################

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setwarnings(False)

################################################################################

from random import random, randrange

try:
    import asyncio
except ImportError:
    # Trollius >= 0.3 was renamed
    import trollius as asyncio
