#-*- coding: utf-8 -*-

from raspberry2use.helpers import *

###############################################################################

@Sparkle.illuminate('raspberry', 'interrupt', "Interruption logicielle", platforms=['raspberry'])
class Interrupt(Crystal):
    def setup(self):
        pass

    def read(self):
        pass

###############################################################################

@Sparkle.illuminate('contact', "Contact logiciel", platforms=['raspberry'])
class Contact(Crystal):
    def setup(self):
        GPIO.setup(self.config['pin'], GPIO.OUT)

        yield self.publish('rpi://jarvis@phenix/relai?key=r1', )

    def read(self):
        yield 'state',

    def write(self):
        GPIO.setup(self.config['pin'], self.config['state'])
